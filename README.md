# ics-ans-role-influxdb

Ansible role to install InfluxDB.

## Role Variables

```yaml
influxdb_directory: /srv/docker/influxdb
influxdb_container_name: influxdb
influxdb_container_image: influxdb:latest
influxdb_dbname: influx
influxdb_orgname: ESS


influxdb_admin_user: admin
influxdb_admin_password: "changeme,iguess"  # And vaultme too!
influxdb_admin_token: "admin_token"  # Vaultme!

telegraf_container_name: telegraf
telegraf_container_image: telegraf:latest
telegraf_deploy: true

telegraf_backup_dir: /influxbackup
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-influxdb
```

## TODO
- Tests!

## License

BSD 2-clause
